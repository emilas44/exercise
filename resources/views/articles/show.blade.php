@extends('layouts.app')

@section('content')

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="/js/edit.js"></script>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-responsive">
				<caption class="text-center"><h1>Article</h1></caption>
				<tr>
    				<th>Article name</th>
    				<th>Article message</th>
    				<th>Article tags</th>

    			</tr>
    			<tr id="{{ $article->id }}">
    				<td class="title">{{ $article->title }}</td>
    				<td class="body">{{ $article->body }}</td>
    				<td class="tags">

					</td>
    			</tr>
			</table>
		</div>
	</div>
</div>

@endsection