
	<div class="form-group">
		{!! Form::label('title', 'Title:') !!}
		{!! Form::text('title', null, ['class' => 'form-control' ]) !!}
	</div>

	<div class="form-group">
		{!! Form::label('body', 'Body:') !!}
		{!! Form::textarea('body', null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('published_at', 'Publish On:') !!}
		{!! Form::text('published_at', null, ['class' => 'date form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('tagList', 'Tags:') !!}
		{!! Form::select('tagList[]', $tags, null, ['class' => 'form-control', 'multiple']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('checked', 'Checkbox:') !!}
		{{-- {!! Form::hidden('checked', 0) !!} --}}
		{{-- {!! Form::checkbox('checked', 0, null, ['class' => 'form-control']) !!} --}}
		{!! Form::hidden('checked', 0) !!}
		{!! Form::checkbox('checked',  1,  isset($article) ? $article->check->checked : 0) !!}
	</div>


	<div class="form-group">
		{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary  form-control']) !!}
	</div>