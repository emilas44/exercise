@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>Articles<a href="/articles/create" class="pull-right btn btn-default">Write new</a></h1>

			<hr>

			@foreach ($articles as $article)
				<div class="well">
				<a href="/articles/{{$article->id}}/edit" class="pull-right btn btn-default">Edit</a>
					<h2>
						<a href="/articles/{{ $article->id }}">{{ $article->title }}</a>
					</h2>
					<p><i>{{ $article->publishedAtHuman }}</i></p>

					<div class="well">{{ $article->body }}</div>

					@unless ($article->tags->isEmpty())	
					<h5>Tags:</h5>
					<ul>				
							@foreach ($article->tags as $tag)
								<li>{{ $tag->name }}</li>
							@endforeach
					</ul>
					@endunless
				</div>
			@endforeach

		</div>
	</div>
</div>

@endsection