@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>Write a new article</h1>

			{!! Form::open(['action' => 'ArticlesController@store']) !!}

				@include('articles._form', ['submitButtonText' => 'Create New Article'])

			{!! Form::close() !!}
			
		</div>
	</div>
</div>

@endsection