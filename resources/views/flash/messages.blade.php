<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="alert alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong> Success! </strong> {{ session('success') }}
                </div>
            @endif

            @if (session('info'))
                <div class="alert alert-info">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong> Info! </strong> {{ session('info') }}
                </div>
            @endif

            @if (session('warning'))
                <div class="alert alert-warning">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong> Warnung! </strong> {{ session('warning') }}
                </div>
            @endif

            @if (session('alert'))
                <div class="alert alert-danger">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong> Fehler: </strong>{{ session('alert') }}
                </div>
            @endif

            @if (count($errors) > 0)
                  <div class="alert alert-danger errors">
                      <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
            @endif
        </div>
    </div>
</div>