$(document).ready(function(){

	$(function() {

		//when a td element within tbody is clicked
		$('tbody').on('click','td',function() {
			//call displayform, passing td jQuery element
			displayForm( $(this) );
		});

	});

	function displayForm( cell ) {

		var column = cell.attr('class'),//class of td corresponds to database table column
			id = cell.closest('tr').attr('id'),//id of tr corresponds to database primary key
			cellWidth = cell.css('width'),//get width of cell for styling width of input field
			prevContent = cell.text(),//store previous value
			token = $('meta[name="csrf-token"]').attr('content'),
			//form action prevents page refresh when enter pressed.  hidden fields pass primary key and column name
			form = '<form action="javascript: this.preventDefault"><input name="_token" type="hidden" value="'+token+'">'+
					'<input type="text" size="4" name="newValue" value="'+
				   	prevContent+'" /><input type="hidden" name="id" value="'+id+'" />'+
				   	'<input type="hidden" name="column" value="'+column+'" /></form>';

		//insert form into td and change focus to input field, set width
		cell.html(form).find('input[type=text]')
			.focus()
			.css('width',cellWidth);

		//disable listener on individual cell once clicked
		cell.on('click', function(){return false});

		//on keypress within td
		cell.on('keydown',function(e) {
			if (e.keyCode == 13) {//13 == enter
				changeField(cell, prevContent);//update field
			} else if (e.keyCode == 27) {//27 == escape
				cell.text(prevContent);//revert to original value
				cell.off('click'); //reactivate editing
			}
		});

		//on out of focus within td
		cell.on('focusout', function() {

			changeField(cell, prevContent);//update field

		});

	}

	function changeField( cell, prevContent ) {

		//remove keydown listener once action initiated
		cell.off('keydown');

		var url = 'ajaxedit',//relative path to PHP processing script
			input = cell.find('form').serialize();//serialize form for passing via url
			
		//send ajax request
		$.ajax({
            //async: false,
            type: "PATCH",
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: input,
            dataType: 'json',
            success: function (data) {
            	console.log(data);
                console.log('success!');
            },
            error: function (data) {
            	console.log(data);
                //alert("danger");
                console.log("danger");
            }
        });

		//remove click handler to allow tbody handler to make field editable again
		cell.off('click');

	}

	$('html').on('click', function() {

		var url = 'ajaxedit';//relative path to PHP processing script

		$.ajax({
            //async: false,
            type: "PATCH",
            url: url,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: {'email':$('input[name=email]').val(), '_token': $('input[name=_token]').val()},
            dataType: 'json',
            success: function (data) {
            	console.log(data);
                console.log('success!');
            },
            error: function (data) {
            	console.log(data);
                //alert("danger");
                console.log("danger");
            }
		});
	});

});