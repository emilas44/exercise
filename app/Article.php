<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\User;
use App\Tag;

class Article extends Model
{
    protected $fillable = [
    	'title',
    	'body',
    	'published_at'
    ];

    protected $dates = ['published_at'];

    public function setPublishedAtAttribute($date)
    {
    	$this->attributes['published_at'] = Carbon::parse($date);
    }

   
    // convert the UTC format to my format
    public function getPublishedAtAttribute($date)
    {    
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d.m.Y H:i');
    }
    
    // get diffForHumans for published_at attribute
    public function getPublishedAtHumanAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['published_at'])->diffForHumans();
    }


    public function scopePublished($query)
    {
    	$query->where('published_at', '<=', Carbon::now());
    }

    public function scopeUnpublished($query)
    {
    	$query->where('published_at', '>', Carbon::now());
    }

    public function user()
    {
         $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function getTagListNamesAttribute()
    {

        $tags = $this->tags->pluck('name');
        return $tags->all();
    }

    public function getTagListAttribute()
    {

        $tags = $this->tags->pluck('id');
        return $tags->all();
    }

    public function check()
    {
        return $this->hasOne(Check::class);
    }
}
