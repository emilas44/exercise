<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\SendNewArticleNotification;
use App\Http\Requests;
use Artisan;
Use DB;

class QueuesController extends Controller
{
    

    public function notifyAllUsers() {

	    $users = \App\User::all()->take(4);

	    //dd($users);
	   	


	   	// this will dispatch a job for every user and variable$user is passed further
	   	
	   	foreach ($users as $user) {
	   		dispatch(new SendNewArticleNotification($user));
	   	}
/*
	   	Artisan::call('queue:work', ['--tries=2']);*/

		/*chdir('..');

		shell_exec('php artisan queue:work --tries=2 >/dev/null 2>/dev/null &');

		while(DB::table('jobs')->count()){

			if(!DB::table('jobs')->count()) break;

			Artisan::call('queue:restart');
		}*/

	   	// run the worker for queue jobs
	   	//chdir('..');
	   	//exec('php artisan queue:work > /dev/null 2 > /dev/null &');


	   	//Artisan::queue('queue:work', ['--option' => 'tries=1']);

	    return redirect('/home')->with('success', 'Sending emails!');


	}
}
