<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Http\Requests;
use App\Article;
use App\Tag;
use Carbon\Carbon;
use Request;
use Auth;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //dd(Carbon::now());
        return view('articles.index', ['articles' => Article::latest('published_at')->published()->get()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all()->pluck('name', 'id')->toArray();

        return view('articles.create', compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        //dd($request->all());

        $article = Auth::user()->articles()->create($request->all());

        $article->check()->create($request->all());

        $article->tags()->sync($request->input('tagList'));


        return redirect('articles')->with('success', "Created new article successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {

        //$article = Article::findOrFail($id);
        //dd($article->publishedAtHuman);

        
        return view('articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {

        $tags = Tag::all()->pluck('name', 'id')->toArray();

        //dd($tags);

        return view('articles.edit', compact('article', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, Article $article)
    {
        

        $article->update($request->all());

        $input = $request->only(['checked']);
        $article->check()->update($input);
        $article->tags()->sync($request->input('tagList'));

        return redirect('articles')->with('success', "Article successfully updated");;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajaxFun(Request $request)
    {
        return response()->json();
        //dd($request->all());
        //return response()->json(
        $column = $_GET['column'];
        $id = $_GET['id'];
        $newValue = $_GET["newValue"];

        $sql = "UPDATE articles SET $column = :value WHERE id = :id";
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':value',$newValue);
        $stmt->bindParam(':id',$id);
        $response['success'] = $stmt->execute();
        $response['value'] = $newValue;

        echo json_encode($response);

    }



}
