<?php

namespace App\Jobs;

use App\Notifications\Published;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewArticleNotification implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    
    // the passed variable instantiated 
    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        // passed variable from controller
        $this->user = $user;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // notify a user with notification Published()
        $this->user->notify(new Published());

    }
}
