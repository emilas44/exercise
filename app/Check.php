<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Check extends Model
{
    

	protected $fillable = ['checked'];

	public function articles()
	{
		return $this->belongsTo(Article::class);
	}
}
