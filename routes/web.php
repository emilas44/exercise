<?php

use App\Notifications\Published;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('403', function () {
    return abort(403);
});

Auth::routes();

Route::get('/home', 'HomeController@index');

// go to user with id and fetch all roles if any
Route::get('/user/{id}', function($id){
	$user = App\User::findOrFail($id);

	foreach ($user->roles as $role) {
		echo $role->id . "<br>";
	}

});

// toggle user role id if he has it...
Route::get('/user/toggle/{id}', function($id){
	$user = App\User::findOrFail(2);

	$user->roles()->toggle($id);

});


// tutorial CRUD
Route::resource('articles', 'ArticlesController');


Route::get('/notify', function () {

	$user = Auth::user();

	$user->notify(new Published());

	return "Email sent";

});


Route::get('/queue', 'QueuesController@notifyAllUsers');




Route::get('/qstart', function () {


	//chdir('..');
    //Artisan::queue('queue:work');
    //Artisan::queue('queue:work', ['--option' => 'tries=1'.' &']);
	//exec('php artisan queue:work --tries=2 &');
	//shell_exec('php artisan queue:work --tries=2 >/dev/null 2>/dev/null &');

    //return redirect('/home')->with('success', 'Sending emails!');


    chdir('..');
    //echo getcwd() . "\n";
    //Artisan::queue('queue:work');
    //Artisan::queue('queue:work', ['--option' => 'daemon']);

	//exec("php artisan queue:work  > /dev/null 2>/dev/null &");
	exec("php artisan queue:work > /dev/null 2 & &");

    return redirect('/home');

});

Route::get('/qstop', function () {


    Artisan::call('queue:restart');

    return 'Queue stopped.';
});


Route::get('/checkjobs', function () {

	while(DB::table('jobs')->count()){

		if(!DB::table('jobs')->count()) break;

		Artisan::call('queue:restart');
	}

	return "Jobs table empty!";
    
});


Route::get('dtables', function () {
    return view('dtables.index');
});


Route::post('ajaxedit', 'ArticlesController@ajaxFun');


Route::get('testing', function () {
    
	$data = App\User::get(['name'])->toArray();

	print_r($data);

});


Route::get('stocks', function () {
	 
	 $symbol  = "GOOG"; 
	 $yahooCSV = "http://finance.yahoo.com/d/quotes.csv?s=$symbol&f=sl1d1t1c1ohgvpnbaejkr&o=t";
	 
	 $csv = fopen($yahooCSV,"r");

	 if($csv) 
	 {
	  list($quote['symbol'], $quote['last'], $quote['date'], $quote['timestamp'], $quote['change'], $quote['open'],
	    $quote['high'], $quote['low'], $quote['volume'], $quote['previousClose'], $quote['name'], $quote['bid'],
	    $quote['ask'], $quote['eps'], $quote['YearLow'], $quote['YearHigh'], $quote['PE']) = fgetcsv($csv, ','); 
	  
	  fclose($csv);
	  
	  print_r($quote); 
	 } 
	 else 
	 {
	  return false;
	 }


});


















