<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // check if table worktypes is empty
		if(DB::table('tags')->get()->count() == 0){

            // multiple insertion
			DB::table('tags')->insert([
				['name' => 'personal', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
	    		['name' => 'programming', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
	    		['name' => 'food', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
	    		['name' => 'work', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
			]);
			
		} else { echo "\e[31mTable is not empty, therefore NOT "; }
    }
}
